# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default value_objects.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# require 'faker'
number_steps = 4

1.upto(number_steps) do |i|
  AuthorizationAchievement.create(title: "Sign in level #{i}",
                                  description: "Authorization in system #{i.to_words} week(s) consecutively",
                                  achievement_group: :everyone,
                                  need_weeks: i)
  # Project.create(title: "Project #{i}", body: "Body #{i} of project #{i}", number_stages: rand(1..10),
  #                start_time: Time.zone.now, finish_time: rand(20..40).days.from_now, status: rand(1..3))
  # AuthorizationDate.create(user_id: User.first.id, date: rand(1..40).days.ago)
end
