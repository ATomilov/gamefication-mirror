class AddDefaultToSuccessConditionToAchievements < ActiveRecord::Migration[5.2]
  def change
    change_column_default :achievements, :success_condition, from: nil, to: {}
  end
end
