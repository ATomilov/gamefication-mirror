class AddNeedFullTimeWeeksColumnToAchievements < ActiveRecord::Migration[5.2]
  def change
    add_column :achievements, :need_weeks, :integer
  end
end
