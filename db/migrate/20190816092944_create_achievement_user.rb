class CreateAchievementUser < ActiveRecord::Migration[5.2]
  def change
    create_table :achievement_users do |t|
      t.integer :achievement_id
      t.integer :user_id

      t.timestamps
    end
  end
end
