class RemoveNeedWeeksColumnFromAchievements < ActiveRecord::Migration[5.2]
  def up
    remove_column :achievements, :need_weeks
  end

  def down
    add_column :achievements, :need_weeks, :integer
  end
end
