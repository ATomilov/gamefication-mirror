class RenameAuthorColumnInContracts < ActiveRecord::Migration[5.2]
  def up
    rename_column :contracts, :author_id, :user_id
  end

  def down
    rename_column :contracts, :user_id, :author_id
  end
end
