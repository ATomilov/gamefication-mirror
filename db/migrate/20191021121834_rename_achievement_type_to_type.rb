class RenameAchievementTypeToType < ActiveRecord::Migration[5.2]
  def up
    rename_column :achievements, :achievement_type, :type
    change_column :achievements, :type, :string, null: false
    change_column_default(:achievements, :type, from: "0", to: nil)
  end

  def down
    change_column_default(:achievements, :type, from: nil, to: "0")
    rename_column :achievements, :type, :achievement_type
  end
end
