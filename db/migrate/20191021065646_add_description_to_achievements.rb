class AddDescriptionToAchievements < ActiveRecord::Migration[5.2]
  def change
    add_column :achievements, :description, :string, null:false
  end
end
