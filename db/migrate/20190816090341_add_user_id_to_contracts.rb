class AddUserIdToContracts < ActiveRecord::Migration[5.2]
  def change
    add_belongs_to :contracts, :user
  end
end
