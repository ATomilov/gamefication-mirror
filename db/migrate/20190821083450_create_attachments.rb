class CreateAttachments < ActiveRecord::Migration[5.2]
  def change
    create_table :attachments do |t|
      t.string :name
      t.string :content_type
      t.integer :size
      t.bigint :attachable_id
      t.string :attachable_type
      t.timestamps
    end
    add_index :attachments, [:attachable_type, :attachable_id]
  end
end
