class AddIndexesToComments < ActiveRecord::Migration[5.2]
  def change
    add_index :comments, [:commentable_id, :commentable_type]
  end
end
