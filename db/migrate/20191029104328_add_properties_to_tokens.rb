class AddPropertiesToTokens < ActiveRecord::Migration[5.2]
  def change
    add_column :integration_service_tokens, :properties, :jsonb, null: false, default: {}
    add_index  :integration_service_tokens, :properties, using: :gin
  end
end
