class CreateAuthorizationDate < ActiveRecord::Migration[5.2]
  def change
    create_table :authorization_dates do |t|
      t.datetime :date, default: -> { 'CURRENT_TIMESTAMP' }, null: false
    end
    add_reference :authorization_dates, :user, index: true
  end
end
