class CreateContracts < ActiveRecord::Migration[5.2]
  def change
    create_table :contracts do |t|
      t.string :start_time
      t.string :finish_time

      t.timestamps
    end
  end
end
