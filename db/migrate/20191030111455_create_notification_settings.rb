class CreateNotificationSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :notification_settings do |t|

      t.jsonb :data, default: {}, null: false
      t.timestamps
    end
  end
end
