class AddProjectAndTokenToNotificationSetting < ActiveRecord::Migration[5.2]
  def change
    add_reference :notification_settings, :project, index: true
    add_reference :notification_settings, :integration_service_token, index: true
  end
end
