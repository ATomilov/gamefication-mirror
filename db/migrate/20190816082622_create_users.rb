class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :patronymic
      t.string :last_name
      t.string :git_token
      t.string :slack_token

      t.timestamps
    end
  end
end
