class AddUserToNotificationSetting < ActiveRecord::Migration[5.2]
  def change
    change_table :notification_settings do |t|
      t.references :author, references: :user
    end
  end
end
