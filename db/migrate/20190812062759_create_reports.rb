class CreateReports < ActiveRecord::Migration[5.2]
  def change
    create_table :reports do |t|
      t.string :header
      t.text :body
      t.integer :amount_time
      t.string :attachment

      t.timestamps
    end
  end
end
