class ChangeComplexIndexInContracts < ActiveRecord::Migration[5.2]
  def up
    remove_index :contracts, column: [:author_id, :project_id]
    add_index :contracts, [:implementer_id, :project_id], unique: true
  end

  def down
    remove_index :contracts, column: [:implementer_id, :project_id]
    add_index :contracts, [:author_id, :project_id], unique: true
  end
end
