class AddNullFalseToTasks < ActiveRecord::Migration[5.2]
  def change
    change_column_null :tasks, :title, false
    change_column_null :tasks, :body, false
    change_column_null :tasks, :start_time, false
    change_column_null :tasks, :finish_time, false
    change_column_null :tasks, :status, false
  end
end
