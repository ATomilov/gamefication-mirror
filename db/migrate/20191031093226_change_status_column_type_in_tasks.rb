class ChangeStatusColumnTypeInTasks < ActiveRecord::Migration[5.2]
  def up
    change_column :tasks, :status, :integer, using: 'status::integer'
  end

  def down
    change_column :tasks, :status, :string
  end
end
