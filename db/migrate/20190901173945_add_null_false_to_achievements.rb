class AddNullFalseToAchievements < ActiveRecord::Migration[5.2]
  def change
    change_column_null :achievements, :title, false
    change_column_null :achievements, :success_condition, false
  end
end
