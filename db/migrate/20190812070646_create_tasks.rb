class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.string :header
      t.text :body
      t.timestamp :start_time
      t.timestamp :finish_time
      t.string :status

      t.timestamps
    end
  end
end
