class AddProjectIdToTasks < ActiveRecord::Migration[5.2]
  def change
    add_belongs_to :tasks, :project
  end
end
