class RemoveAttachmentsColumnInProjectsAndReports < ActiveRecord::Migration[5.2]
  def change
    remove_column :reports, :attachment, :string
    remove_column :projects, :attachment, :string
  end
end
