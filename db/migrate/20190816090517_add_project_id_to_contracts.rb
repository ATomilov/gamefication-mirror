class AddProjectIdToContracts < ActiveRecord::Migration[5.2]
  def change
    add_belongs_to :contracts, :project
  end
end
