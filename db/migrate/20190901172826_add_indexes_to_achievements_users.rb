class AddIndexesToAchievementsUsers < ActiveRecord::Migration[5.2]
  def change
    add_index :achievement_users, [:user_id, :achievement_id], unique: true
  end
end
