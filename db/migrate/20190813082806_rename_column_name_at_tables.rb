class RenameColumnNameAtTables < ActiveRecord::Migration[5.2]
  def change
    rename_column :projects, :header, :title
    rename_column :reports, :header, :title
    rename_column :tasks, :header, :title
  end
end
