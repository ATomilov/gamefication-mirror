class RemoveDefaultValueFromActivityStatistic < ActiveRecord::Migration[5.2]
  def change
    change_column_default(:activity_statistics, :type, from: 0, to: nil)
  end
end
