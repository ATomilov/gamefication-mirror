class ChangeTypeRoleInUsersTable < ActiveRecord::Migration[5.2]
  def up
    change_column_default :users, :role, from: 'Developer', to: nil
    change_column :users, :role, 'integer USING CAST(role AS integer)'
    change_column_default :users, :role, from: nil, to: 0
  end

  def down
    change_column :users, :role, 'varchar USING CAST(role AS varchar)'
    change_column_default :users, :role, from: nil, to: '0'
  end
end
