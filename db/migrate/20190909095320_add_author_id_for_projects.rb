class AddAuthorIdForProjects < ActiveRecord::Migration[5.2]
  def change
    change_table :projects do |t|
      t.references :author, references: :user
    end
  end
end
