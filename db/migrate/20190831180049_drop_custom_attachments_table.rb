class DropCustomAttachmentsTable < ActiveRecord::Migration[5.2]
  def change
    remove_index :attachments, column: [:attachable_type, :attachable_id]
    drop_table :attachments do |t|
      t.string :name
      t.string :content_type
      t.integer :size
      t.bigint :attachable_id
      t.string :attachable_type
      t.timestamps
    end
  end
end
