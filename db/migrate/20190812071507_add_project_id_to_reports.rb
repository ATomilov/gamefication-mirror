class AddProjectIdToReports < ActiveRecord::Migration[5.2]
  def change
    add_belongs_to :reports, :project
  end
end
