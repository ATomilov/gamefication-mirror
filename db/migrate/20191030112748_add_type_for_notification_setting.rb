class AddTypeForNotificationSetting < ActiveRecord::Migration[5.2]
  def change
    add_column :notification_settings, :type, :string, null:false
  end
end
