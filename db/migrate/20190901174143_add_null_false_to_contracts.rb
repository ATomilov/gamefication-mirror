class AddNullFalseToContracts < ActiveRecord::Migration[5.2]
  def change
    change_column_null :contracts, :start_time, false
    change_column_null :contracts, :finish_time, false
  end
end
