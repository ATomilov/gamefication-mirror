class AddNullFalseToProjects < ActiveRecord::Migration[5.2]
  def change
    change_column_null :projects, :title, false
    change_column_null :projects, :body, false
    change_column_null :projects, :number_stages, false
    change_column_null :projects, :start_time, false
    change_column_null :projects, :finish_time, false
    change_column_null :projects, :status, false
  end
end
