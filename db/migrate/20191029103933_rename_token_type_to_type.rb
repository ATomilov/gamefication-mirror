class RenameTokenTypeToType < ActiveRecord::Migration[5.2]
  def up
    rename_column :integration_service_tokens, :token_type, :type
    change_column :integration_service_tokens, :type, :string, null: false
    change_column_default(:integration_service_tokens, :type, from: "0", to: nil)
  end

  def down
    change_column_default(:integration_service_tokens, :type, from: nil, to: "0")
    rename_column :integration_service_tokens, :type, :token_type
  end
end
