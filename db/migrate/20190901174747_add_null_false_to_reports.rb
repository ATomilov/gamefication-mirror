class AddNullFalseToReports < ActiveRecord::Migration[5.2]
  def change
    change_column_null :reports, :title, false
    change_column_null :reports, :body, false
    change_column_null :reports, :amount_time, false
  end
end
