class AddImplementerIdToContracts < ActiveRecord::Migration[5.2]
  def change
    change_table :contracts do |t|
      t.references :implementer, references: :user
    end
  end
end
