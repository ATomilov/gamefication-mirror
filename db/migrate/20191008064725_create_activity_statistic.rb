class CreateActivityStatistic < ActiveRecord::Migration[5.2]
  def change
    create_table :activity_statistics do |t|
      t.string :type, :string, null: false, default: ''
      t.jsonb :data, default: {}, null: false
    end
    add_reference :activity_statistics, :user, index: true
    add_index :activity_statistics, :data, using: :gin
  end
end
