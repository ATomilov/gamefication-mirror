class RenameActivityStatisticsToStatistics < ActiveRecord::Migration[5.2]
  def self.up
    rename_table :activity_statistics, :statistics
  end

  def self.down
    rename_table :statistics, :activity_statistics
  end
end
