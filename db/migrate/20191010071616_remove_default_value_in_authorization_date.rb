class RemoveDefaultValueInAuthorizationDate < ActiveRecord::Migration[5.2]
  def change
    change_column_default(:authorization_dates, :date, from: -> { "CURRENT_TIMESTAMP" }, to: nil)
  end
end
