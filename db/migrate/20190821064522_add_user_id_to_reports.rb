class AddUserIdToReports < ActiveRecord::Migration[5.2]
  def change
    add_belongs_to :reports, :user
  end
end
