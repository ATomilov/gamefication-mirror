class ChangeColumnSuccessConditionInAchievements < ActiveRecord::Migration[5.2]
  def up
    change_column :achievements, :success_condition, :jsonb, using: 'success_condition::jsonb'
  end

  def down
    change_column :achievements, :success_condition, :string, using: 'success_condition::text'
  end
end
