class AddAuthorIdForComments < ActiveRecord::Migration[5.2]
  def change
    change_table :comments do |t|
      t.references :author, references: :user, null: false
    end
  end
end
