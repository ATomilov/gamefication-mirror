class RemoveTokensColumnsInUsers < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :git_token, :string
    remove_column :users, :slack_token, :string
  end
end
