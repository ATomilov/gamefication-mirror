class CreateIntegrationServiceTokens < ActiveRecord::Migration[5.2]
  def change
    create_table :integration_service_tokens do |t|
      t.string :token_body, null: false
      t.integer :token_type, null: false
      t.timestamps
    end
    add_belongs_to :integration_service_tokens, :user
  end
end
