class AddIndexesToContracts < ActiveRecord::Migration[5.2]
  def change
    add_index :contracts, [:user_id, :project_id], unique: true
  end
end
