class ChangeColumnTimeTypeInContracts < ActiveRecord::Migration[5.2]
  def up
    rename_column :contracts, :start_time, :start_time_back
    rename_column :contracts, :finish_time, :finish_time_back
    add_column :contracts, :start_time, :datetime
    add_column :contracts, :finish_time, :datetime
    remove_column :contracts, :start_time_back, :string
    remove_column :contracts, :finish_time_back, :string
  end

  def down
    add_column :contracts, :start_time_back, :string
    add_column :contracts, :finish_time_back, :string
    remove_column :contracts, :start_time, :datetime
    remove_column :contracts, :finish_time, :datetime
    rename_column :contracts, :start_time_back, :start_time
    rename_column :contracts, :finish_time_back, :finish_time
  end
end
