class AddGroupAndTypeToAchievements < ActiveRecord::Migration[5.2]
  def change
    add_column :achievements, :achievement_group, :integer, default: 0, null: false
    add_column :achievements, :achievement_type, :integer, default: 0, null: false
  end
end
