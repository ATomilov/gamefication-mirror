class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string :header
      t.text :body
      t.string :attachment
      t.integer :number_stages
      t.timestamp :start_time
      t.timestamp :finish_time
      t.string :status

      t.timestamps
    end
  end
end
