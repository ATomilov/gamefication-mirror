class AddImplementerIdToTasks < ActiveRecord::Migration[5.2]
  def change
    change_table :tasks do |t|
      t.references :implementer, references: :user
    end
  end
end
