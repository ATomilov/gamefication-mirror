# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_11_18_125900) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "achievement_users", force: :cascade do |t|
    t.integer "achievement_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id", "achievement_id"], name: "index_achievement_users_on_user_id_and_achievement_id", unique: true
  end

  create_table "achievements", force: :cascade do |t|
    t.string "title", null: false
    t.jsonb "success_condition", default: {}, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "achievement_group", default: 0, null: false
    t.string "type", null: false
    t.string "description", null: false
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "authorization_dates", force: :cascade do |t|
    t.datetime "date", null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_authorization_dates_on_user_id"
  end

  create_table "comments", force: :cascade do |t|
    t.string "content", null: false
    t.integer "commentable_id"
    t.string "commentable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "author_id", null: false
    t.index ["author_id"], name: "index_comments_on_author_id"
    t.index ["commentable_id", "commentable_type"], name: "index_comments_on_commentable_id_and_commentable_type"
  end

  create_table "contracts", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.bigint "project_id"
    t.datetime "start_time", null: false
    t.datetime "finish_time", null: false
    t.bigint "implementer_id"
    t.index ["implementer_id", "project_id"], name: "index_contracts_on_implementer_id_and_project_id", unique: true
    t.index ["implementer_id"], name: "index_contracts_on_implementer_id"
    t.index ["project_id"], name: "index_contracts_on_project_id"
    t.index ["user_id"], name: "index_contracts_on_user_id"
  end

  create_table "integration_service_tokens", force: :cascade do |t|
    t.string "token_body", null: false
    t.string "type", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.jsonb "properties", default: {}, null: false
    t.index ["properties"], name: "index_integration_service_tokens_on_properties", using: :gin
    t.index ["user_id"], name: "index_integration_service_tokens_on_user_id"
  end

  create_table "notification_settings", force: :cascade do |t|
    t.jsonb "data", default: {}, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "project_id"
    t.bigint "integration_service_token_id"
    t.string "type", null: false
    t.bigint "author_id"
    t.index ["author_id"], name: "index_notification_settings_on_author_id"
    t.index ["integration_service_token_id"], name: "index_notification_settings_on_integration_service_token_id"
    t.index ["project_id"], name: "index_notification_settings_on_project_id"
  end

  create_table "projects", force: :cascade do |t|
    t.string "title", null: false
    t.text "body", null: false
    t.integer "number_stages", null: false
    t.datetime "start_time", null: false
    t.datetime "finish_time", null: false
    t.integer "status", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "author_id"
    t.index ["author_id"], name: "index_projects_on_author_id"
  end

  create_table "reports", force: :cascade do |t|
    t.string "title", null: false
    t.text "body", null: false
    t.integer "amount_time", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "project_id"
    t.bigint "user_id"
    t.index ["project_id"], name: "index_reports_on_project_id"
    t.index ["user_id"], name: "index_reports_on_user_id"
  end

  create_table "statistics", force: :cascade do |t|
    t.string "type", null: false
    t.jsonb "data", default: {}, null: false
    t.bigint "user_id"
    t.index ["data"], name: "index_statistics_on_data", using: :gin
    t.index ["user_id"], name: "index_statistics_on_user_id"
  end

  create_table "tasks", force: :cascade do |t|
    t.string "title", null: false
    t.text "body", null: false
    t.datetime "start_time", null: false
    t.datetime "finish_time", null: false
    t.integer "status", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "project_id"
    t.bigint "user_id"
    t.bigint "implementer_id"
    t.index ["implementer_id"], name: "index_tasks_on_implementer_id"
    t.index ["project_id"], name: "index_tasks_on_project_id"
    t.index ["user_id"], name: "index_tasks_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name", null: false
    t.string "patronymic", null: false
    t.string "last_name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "role", default: 0, null: false
    t.string "email"
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
end
