# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Gamefication
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    config.application = config_for(:application)

    config.active_job.queue_adapter = :sidekiq

    config.i18n.default_locale = :en

    config.autoload_paths += %W(#{config.root}/app/models/statistics)
    config.autoload_paths += %W(#{config.root}/app/models/achievements)
    config.autoload_paths += %W(#{config.root}/app/models/integration_service_tokens)
    config.autoload_paths += %W(#{config.root}/app/models/notification_settings)
  end
end
