# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users
  # devise_for :managers
  # devise_for :developers
  #
  require 'sidekiq/web'
  require 'sidekiq/cron/web'
  mount Sidekiq::Web => '/sidekiq'

  root 'welcome#index'

  resources :users, only: %i[index show destroy] do
    resources :achievements, only: %i[index]
    resources :contracts, only: %i[index]
  end

  resources :achievements

  resources :attachments, only: %i[destroy]

  resources :comments, except: %i[show index] do
    resources :comments, shallow: true, except: %i[show index]
  end

  resources :contracts

  resources :notification_settings

  resources :projects

  resources :reports

  resources :tasks do
    resources :comments, shallow: true, except: %i[show index]
  end

  authenticate :user, -> (user) { user.admin? || user.manager? } do
    get '/auth/callback', to: 'slack#callback'
    get '/slack_token', to: 'pages#slack_token'
  end
end
