ActionMailer::Base.smtp_settings = {
  domain: 'morning-wildwood-94216.herokuapp.com',
  address: "smtp.sendgrid.net",
  port: 587,
  authentication: :plain,
  user_name: 'apikey',
  password: Rails.application.credentials.send_grid[:api_key]
}