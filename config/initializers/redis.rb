if Rails.env.test?
  Redis.current = Redis::Namespace.new("gamefication", redis: Redis.new(db: 1))
elsif Rails.env.development?
  Redis.current = Redis::Namespace.new("gamefication", redis: Redis.new(db: 15))
else
  Redis.current = Redis::Namespace.new("morning-wildwood-94216", redis: Redis.new(db: 1))
end