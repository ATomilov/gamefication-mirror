# frozen_string_literal: true

RSpec.configure do |config|
  config.before { Redis.current.redis.flushdb }
  config.after { Redis.current.redis.quit }
end
