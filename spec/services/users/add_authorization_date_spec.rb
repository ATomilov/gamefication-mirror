# frozen_string_literal: true

require 'rails_helper'

describe Users::AddAuthorizationDate do
  subject { described_class.call(user) }

  let(:user) { create(:user) }
  let(:type) { 'authorization' }
  let(:data_keys_values) do
    { consecutive_days: 0,
      last_one_week_days: 1,
      last_two_weeks_days: 1,
      last_three_weeks_days: 1,
      last_four_weeks_days: 1,
      last_one_week_consecutive_weeks: 0,
      last_two_week_consecutive_weeks: 0,
      last_three_week_consecutive_weeks: 0,
      last_four_week_consecutive_weeks: 0 }
  end
  let(:auth_date) { create(:authorization_date) }

  shared_examples 'create statistic record' do
    it 'calls create authorization date record' do
      expect(auth_date.date).to eq(Time.current.change(usec: 0))
    end

    it 'calls saving statistic record to db' do
      expect(Statistics::Create).to receive(:call).with(user, type, data_keys_values)
      subject
    end

    it 'increase authorization dates count by 1' do
      expect { subject }.to change { AuthorizationDate.count }.by(1)
    end
  end

  shared_examples 'not create statistic record' do
    it 'return nil' do
      allow_any_instance_of(described_class).to receive(:user_was_active_today?).and_return(true)
      expect(described_class.call(user)).to be_nil
    end

    it 'not increase statistics count' do
      allow_any_instance_of(described_class).to receive(:user_was_active_today?).and_return(true)
      expect { subject }.not_to change { Statistic.count }.from(0)
    end
  end

  context 'when user was active today' do
    it_behaves_like 'not create statistic record'
  end

  context 'when user was not active today' do
    it_behaves_like 'create statistic record'
  end
end
