# frozen_string_literal: true

require 'rails_helper'

describe Statistics::Create do
  subject { described_class.call(user, type, data_keys_values) }

  let(:user) { create(:user) }
  let(:type) { 'authorization' }
  let(:data_keys_values) do
    { last_one_week_days: 1,
      last_two_weeks_days: 1,
      last_three_weeks_days: 1,
      last_four_weeks_days: 1,
      consecutive_days: 1 }
  end
  let(:auth_statistic) { create(:authorization_statistic) }

  shared_examples 'create statistic record' do
    it 'calls saving statistic record to db' do
      expect(described_class).to receive(:call).with(user, type, data_keys_values)
      subject
    end

    it 'increase statistics count by 1' do
      expect { subject }.to change { Statistic.count }.by(1)
    end
  end

  shared_examples 'not create statistic record' do
    it 'statistic record already exist' do
      expect(AuthorizationStatistic.where(id: auth_statistic.id)).to exist
    end
  end

  shared_examples 'type invalid' do
    it 'return nil' do
      expect(described_class.call(user, type, data_keys_values)).to be_nil
      subject
    end
  end

  context 'when type not exist' do
    let(:type) { 'not_existing_class' }

    it_behaves_like 'type invalid'
  end

  context 'when type exist' do
    context 'when statistic record exist' do
      it_behaves_like 'not create statistic record'
    end

    context 'when statistic record not exist' do
      it_behaves_like 'create statistic record'
    end
  end
end
