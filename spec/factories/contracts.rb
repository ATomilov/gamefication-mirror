# frozen_string_literal: true

FactoryBot.define do
  factory :contract do
    user_id { current_user.id }
    implementer_id { User.second.id }
    start_time { Date.current }
    finish_time  { Date.current + 3.days }
    project_id { Project.first.id }
  end
end
