# frozen_string_literal: true

FactoryBot.define do
  factory :project do
    title { Faker::Book.title }
    body { Faker::Lorem.sentence }
    number_stages { rand(1..10) }
    start_time { Date.current }
    finish_time { Date.current + 3.days }
    status { rand(1..3) }
  end
end
