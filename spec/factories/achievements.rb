# frozen_string_literal: true

FactoryBot.define do
  factory :achievement, class: 'Achievement' do
    title { Faker::Book.title }
    description { Faker::Lorem.sentence }
  end

  factory :authorization_achievement, parent: :achievement, class: 'AuthorizationAchievement' do
    type { 'AuthorizationAchievement' }
  end
end
