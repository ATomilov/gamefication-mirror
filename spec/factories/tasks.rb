# frozen_string_literal: true

FactoryBot.define do
  factory :task do
    title { Faker::Book.title }
    body { Faker::Lorem.sentence }
    start_time { Date.current }
    finish_time { Date.current + 2.days }
    status { 'open' }
  end
end
