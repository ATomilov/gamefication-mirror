# frozen_string_literal: true

FactoryBot.define do
  factory :authorization_date do
    association :user, factory: :user
    date { Time.current.change(usec: 0) }
  end
end
