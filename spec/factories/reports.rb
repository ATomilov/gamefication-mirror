# frozen_string_literal: true

FactoryBot.define do
  factory :report do
    title { Faker::Book.title }
    body { Faker::Lorem.sentence }
    amount_time { rand(100..1000) }
  end
end
