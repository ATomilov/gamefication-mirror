# frozen_string_literal: true

require 'rails_helper'

describe Comment do
  describe 'validations' do
    subject { FactoryBot.build(:comment) }

    it { is_expected.to validate_presence_of(:content) }
  end
end
