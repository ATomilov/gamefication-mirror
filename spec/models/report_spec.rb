# frozen_string_literal: true

require 'rails_helper'

describe Report do
  describe 'validations' do
    subject { FactoryBot.build(:report) }

    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_length_of(:title).is_at_least(5) }
    it { is_expected.to validate_presence_of(:body) }
    it { is_expected.to validate_presence_of(:amount_time) }
  end
end
