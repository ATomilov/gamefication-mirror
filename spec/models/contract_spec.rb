# frozen_string_literal: true

require 'rails_helper'

describe Contract do
  describe 'validations' do
    it { is_expected.to validate_presence_of(:start_time) }
    it { is_expected.to validate_presence_of(:finish_time) }
    # subject { FactoryBot.create(:contract) }
    # it { should validate_uniqueness_of(:implementer).scoped_to(:project) }
  end
end
