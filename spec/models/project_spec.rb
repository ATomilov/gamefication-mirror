# frozen_string_literal: true

require 'rails_helper'

describe Project do
  describe 'validations' do
    subject { FactoryBot.build(:project) }

    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_length_of(:title).is_at_least(5) }
    it { is_expected.to validate_uniqueness_of(:title) }
    it { is_expected.to validate_presence_of(:body) }
    it { is_expected.to validate_presence_of(:number_stages) }
    it { is_expected.to validate_presence_of(:start_time) }
    it { is_expected.to validate_presence_of(:finish_time) }
    it { is_expected.to validate_presence_of(:status) }
  end
end
