# frozen_string_literal: true

require 'rails_helper'

describe Task do
  describe 'validations' do
    subject { FactoryBot.build(:task) }

    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_length_of(:title).is_at_least(5) }
    it { is_expected.to validate_presence_of(:body) }
    it { is_expected.to validate_presence_of(:start_time) }
    it { is_expected.to validate_presence_of(:finish_time) }
    it { is_expected.to validate_presence_of(:status) }
  end
end
