# frozen_string_literal: true

require 'rails_helper'

describe AuthorizationDate do
  describe 'relation' do
    it { is_expected.to belong_to(:user) }
  end

  describe 'columns' do
    it { is_expected.to have_db_column(:user_id) }
    it { is_expected.to have_db_column(:date) }
    it { is_expected.to have_db_index(:user_id) }
  end
end
