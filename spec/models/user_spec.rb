# frozen_string_literal: true

require 'rails_helper'

describe User do
  describe 'validations' do
    subject { FactoryBot.build(:user) }

    it { is_expected.to validate_presence_of(:first_name) }
    it { is_expected.to validate_presence_of(:patronymic) }
    it { is_expected.to validate_presence_of(:last_name) }
  end

  describe 'relations' do
    it { is_expected.to have_many(:authorization_dates).dependent(:destroy) }
    it { is_expected.to have_one(:authorization_statistic).dependent(:destroy) }
  end
end
