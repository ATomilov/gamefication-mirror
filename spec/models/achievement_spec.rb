# frozen_string_literal: true

require 'rails_helper'

describe Achievement do
  describe 'validations' do
    subject { FactoryBot.build(:authorization_achievement) }

    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_length_of(:title).is_at_least(5) }
    it { is_expected.to validate_uniqueness_of(:title) }
    it { is_expected.to validate_presence_of(:description) }
    it { is_expected.not_to allow_value("NotAchievementType").for(:type) }
  end

  describe 'columns' do
    it { is_expected.to have_db_column(:title).of_type(:string) }
    it { is_expected.to have_db_column(:success_condition).of_type(:jsonb) }
    it { is_expected.to have_db_column(:description).of_type(:string) }
    it { is_expected.to have_db_column(:achievement_group).of_type(:integer) }
    it { is_expected.to have_db_column(:type).of_type(:string) }
  end
end
