# frozen_string_literal: true

namespace :users do
  task :convert_role do
    User.find_each do |user|
      role = if user.role == 'Developer'
               '0'
             elsif user.role == 'Manager'
               '1'
             elsif user.role == 'Admin'
               '2'
             end
      user.update(role: role)
    end
  end
end
