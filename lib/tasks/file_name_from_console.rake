# frozen_string_literal: true

require 'optparse'

task :file_name_from_console do
  # %x{ sed -e '$s/$/\\n/' -s .env.development.local > finalfile.txt
  #   cat finalfile.txt | cut -f1 -d"=" > finalfile2.txt
  #   sed -i '/^$/d' finalfile2.txt
  #   sed -i 's/$/=/' finalfile2.txt
  #   awk '!x[$0]++' finalfile2.txt > finalfile3.txt
  #   sort finalfile3.txt > finalfile4.txt
  #   cat finalfile4.txt > .env.development
  #   rm finalfile*
  # }

  options = {}

  o = OptionParser.new

  o.banner = "Usage: rake sync_env_file [options]"
  o.on('--from FILE') do |name|
    options[:from_file] = name
  end
  o.on('--to FILE') do |name|
    options[:to_file] = name
  end

  # return `ARGV` with the intended arguments
  args = o.order!(ARGV) {}

  o.parse!(args)

  # puts "hello #{options[:name]}"
  p options
  ` sed -e '$s/$/\\n/' -s #{options[:from_file]} > finalfile.txt
    cat finalfile.txt | cut -f1 -d"=" > finalfile2.txt
    sed -i '/^$/d' finalfile2.txt
    sed -i 's/$/=/' finalfile2.txt
    awk '!x[$0]++' finalfile2.txt > finalfile3.txt
    sort finalfile3.txt > finalfile4.txt
    cat finalfile4.txt > #{options[:to_file]}
    rm finalfile*
  `
  exit
end
