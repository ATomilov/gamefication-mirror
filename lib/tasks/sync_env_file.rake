# frozen_string_literal: true

require 'optparse'

task :sync_env_file do
  options = {}

  o = OptionParser.new

  o.banner = "Usage: rake sync_env_file [options]"
  o.on('--from FILE') do |name|
    options[:from_file] = name
  end
  o.on('--to FILE') do |name|
    options[:to_file] = name
  end

  args = o.order!(ARGV) {}

  o.parse!(args)

  p options
  ` sed -e '$s/$/\\n/' -s #{options[:from_file]} > finalfile.txt
    cat finalfile.txt | cut -f1 -d"=" > finalfile2.txt
    sed -i '/^$/d' finalfile2.txt
    sed -i 's/$/=/' finalfile2.txt
    awk '!x[$0]++' finalfile2.txt > finalfile3.txt
    sort finalfile3.txt > finalfile4.txt
    cat finalfile4.txt >> #{options[:to_file]}
    awk '!x[$0]++' #{options[:to_file]} > #{options[:to_file]}_temp
    sort #{options[:to_file]}_temp > #{options[:to_file]}_temp2
    cat #{options[:to_file]}_temp2 > #{options[:to_file]}
    rm finalfile*
    rm #{options[:to_file]}_temp*
  `
  exit
end
