# frozen_string_literal: true

namespace :achievements do
  task :set_group_type do
    Achievement.find_each do |achievement|
      achievement.update(achievement_group: 0, achievement_type: 0)
    end
  end
end
