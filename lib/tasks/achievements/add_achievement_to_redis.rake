# frozen_string_literal: true

namespace :achievements do
  task :add_to_redis do
    Achievement.find_each do |achievement|
      group = if achievement.for_everyone?
                'everyone'
              elsif achievement.for_developer?
                'developer'
              elsif achievement.for_manager?
                'manager'
              end
      type = if achievement.untyped?
               'untyped'
             elsif achievement.authorization?
               'authorization'
             end
      Redis.current.hmset("achievement:#{achievement.id}",
                          Achievements::RedisHashKeys.title, achievement.title,
                          Achievements::RedisHashKeys.group, group,
                          Achievements::RedisHashKeys.type, type,
                          Achievements::RedisHashKeys.need_full_time_weeks, 0)
    end
  end
end
