# frozen_string_literal: true

class UserPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.admin?
        scope.all
      elsif user.manager?
        scope.where(manager.or(developer))
      else
        scope.where(developer)
      end
    end

    private

    def scope_arel_table
      scope.arel_table
    end

    def manager
      scope_arel_table[:role].eq(User.roles[:manager])
    end

    def developer
      scope_arel_table[:role].eq(User.roles[:developer])
    end
  end

  def index?
    true
  end

  def show?
    index?
  end

  def new?
    false
  end

  def update?
    false
  end

  def destroy?
    user.admin?
  end

  def create?
    new?
  end

  def edit?
    update?
  end
end
