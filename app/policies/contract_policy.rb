# frozen_string_literal: true

class ContractPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.admin?
        scope.all
      elsif user.manager?
        scope.where(author.or(implementer))
      else
        scope.where(implementer)
      end
    end

    private

    def scope_arel_table
      scope.arel_table
    end

    def author
      scope_arel_table[:user_id].eq(user.id)
    end

    def implementer
      scope_arel_table[:implementer_id].eq(user.id)
    end
  end

  def index?
    true
  end

  def show?
    new?
  end

  def new?
    user.admin? || user.manager?
  end

  def update?
    user.admin? || owner?
  end

  def destroy?
    user.admin? || owner?
  end

  def create?
    new?
  end

  def edit?
    update?
  end

  private

  def owner?
    user.id == record.user_id
  end
end
