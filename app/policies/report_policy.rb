# frozen_string_literal: true

class ReportPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.admin?
        scope.all
      else
        scope.where(user_id: user.id)
      end
    end
  end

  def index?
    true
  end

  def show?
    index?
  end

  def new?
    user.admin? || user.manager? || user.developer?
  end

  def update?
    user.admin? || owner?
  end

  def destroy?
    user.admin? || owner?
  end

  def create?
    new?
  end

  def edit?
    update?
  end

  private

  def owner?
    user.id == record.user_id
  end
end
