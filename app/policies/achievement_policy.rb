# frozen_string_literal: true

class AchievementPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    index?
  end

  def new?
    user.admin?
  end

  def update?
    user.admin?
  end

  def destroy?
    user.admin?
  end

  def create?
    new?
  end

  def edit?
    update?
  end
end
