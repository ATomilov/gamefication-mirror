# frozen_string_literal: true

class IntegrationServiceTokenPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.admin?
        scope.all
      elsif user.manager?
        scope.where(owner)
      else
        scope.none
      end
    end

    private

    def scope_arel_table
      scope.arel_table
    end

    def owner
      scope_arel_table[:user_id].eq(user.id)
    end
  end

  def index?
    new?
  end

  def show?
    new?
  end

  def new?
    user.admin? || user.manager?
  end

  def update?
    user.admin? || owner?
  end

  def destroy?
    user.admin? || owner?
  end

  def create?
    new?
  end

  def edit?
    update?
  end

  private

  def owner?
    user.id == record.user_id
  end
end
