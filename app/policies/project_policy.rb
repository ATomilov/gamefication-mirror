# frozen_string_literal: true

class ProjectPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.admin?
        scope.all
      elsif user.manager?
        implementer.or(author)
      else
        implementer
      end
    end

    private

    def scope_with_users
      scope.left_joins(:implementers)
    end

    def implementer
      scope_with_users.where(contracts: { implementer_id: user.id })
    end

    def author
      scope_with_users.where(projects: { author_id: user.id })
    end
  end

  def index?
    true
  end

  def show?
    index?
  end

  def new?
    user.admin? || user.manager?
  end

  def update?
    user.admin? || owner?
  end

  def destroy?
    user.admin? || owner?
  end

  def create?
    new?
  end

  def edit?
    update?
  end

  private

  def owner?
    user.id == record.author_id
  end
end
