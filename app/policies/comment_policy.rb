# frozen_string_literal: true

class CommentPolicy < ApplicationPolicy
  def index?
    true
  end

  def show?
    new?
  end

  def new?
    user.admin? || user.manager? || user.developer?
  end

  def update?
    user.admin? || owner?
  end

  def destroy?
    user.admin?
  end

  def create?
    new?
  end

  def edit?
    update?
  end

  private

  def owner?
    user.id == record.author_id
  end
end
