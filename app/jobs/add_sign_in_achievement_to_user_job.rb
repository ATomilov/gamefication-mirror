# frozen_string_literal: true

class AddSignInAchievementToUserJob < ApplicationJob
  queue_as :default

  def perform(user_id)
    Achievements::SignInAchievement.call(user_id)
  end
end
