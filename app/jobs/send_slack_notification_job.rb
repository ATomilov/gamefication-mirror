# frozen_string_literal: true

class SendSlackNotificationJob < ApplicationJob
  queue_as :default

  def perform(url, data)
    SendSlackNotificationService.call(url, data)
  end
end
