# frozen_string_literal: true

class AddAchievementsToUsersJob < ApplicationJob
  queue_as :default

  def perform(*_args)
    User.find_each do |user|
      AddSignInAchievementToUserJob.perform_later(user.id)
    end
  end
end
