# frozen_string_literal: true

class SlackTokenDecorator < IntegrationServiceTokenDecorator
  def channel_with_workspace
    "#{object.properties[:channel]} (#{object.properties[:team_name]})"
  end
end
