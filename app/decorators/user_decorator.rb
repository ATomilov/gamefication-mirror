# frozen_string_literal: true

class UserDecorator < Draper::Decorator
  delegate_all
  decorates_finders

  def self.collection_decorator_class
    PaginatingDecorator
  end

  def name_with_initial
    "#{user.first_name.first}. #{user.patronymic.first}. #{user.last_name}"
  end

  def full_name_with_postfix
    full_name_with_initials + (current_user? ? " #{I18n.t('users_decorator.prefix')}" : "")
  end

  private

  def current_user?
    helpers.current_user == user
  end

  def full_name_with_initials
    "#{user.first_name.first}. #{user.patronymic.first}. #{user.last_name}"
  end
end
