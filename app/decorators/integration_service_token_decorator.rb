# frozen_string_literal: true

class IntegrationServiceTokenDecorator < Draper::Decorator
  delegate_all
  decorates_finders
end
