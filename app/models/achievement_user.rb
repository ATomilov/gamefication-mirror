# frozen_string_literal: true

class AchievementUser < ApplicationRecord
  belongs_to :achievement
  belongs_to :user

  validates :achievement, uniqueness: { scope: :user }
end
