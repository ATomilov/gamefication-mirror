# frozen_string_literal: true

class Task < ApplicationRecord
  include Commentable

  belongs_to :project
  belongs_to :user
  belongs_to :implementer, class_name: 'User'
  has_many_attached :files

  validates :title, presence: true, length: { minimum: 5 }, uniqueness: true
  validates :body, presence: true
  validates :start_time, presence: true
  validates :finish_time, presence: true
  validates :status, presence: true

  delegate :title, to: :project, prefix: true, allow_nil: true

  validates_with DateValidator

  enum status: { open: 0, solved: 1, closed: 2 }
end
