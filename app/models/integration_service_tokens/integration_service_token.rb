# frozen_string_literal: true

class IntegrationServiceToken < ApplicationRecord
  include Stiable

  serialize :properties, HashSerializer

  belongs_to :user
  has_one :notification_setting, dependent: :destroy
end
