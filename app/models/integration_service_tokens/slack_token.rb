# frozen_string_literal: true

class SlackToken < IntegrationServiceToken
  store_accessor :properties, %i[team_id team_name webhook_url channel]
end
