# frozen_string_literal: true

class Statistic < ApplicationRecord
  include Stiable

  serialize :data, HashSerializer

  belongs_to :user
end
