# frozen_string_literal: true

class AuthorizationStatistic < Statistic
  store_accessor :data, %i[last_one_week_days last_two_weeks_days
                           last_three_weeks_days last_four_weeks_days consecutive_days last_one_week_consecutive_weeks
                           last_two_week_consecutive_weeks last_three_week_consecutive_weeks
                           last_four_week_consecutive_weeks]
end
