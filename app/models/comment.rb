# frozen_string_literal: true

class Comment < ApplicationRecord
  include Commentable

  belongs_to :commentable, polymorphic: true
  belongs_to :author, class_name: 'User', foreign_key: :author_id, inverse_of: :comments
  has_many_attached :files

  validates :content, presence: true

  delegate :role, to: :author, prefix: true, allow_nil: true

  def parent
    commentable if commentable.is_a? Comment
  end
end
