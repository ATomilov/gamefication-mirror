# frozen_string_literal: true

class Project < ApplicationRecord
  has_many :reports, dependent: :destroy
  has_many :tasks, dependent: :destroy
  has_many :contracts, dependent: :destroy
  has_many :implementers, through: :contracts, class_name: 'User', foreign_key: :implementer_id,
                          inverse_of: :implementer_projects
  has_many :slack_notification_settings, dependent: :destroy
  has_many_attached :files

  belongs_to :author, class_name: 'User'

  validates :title, presence: true, length: { minimum: 5 }, uniqueness: true
  validates :body, presence: true
  validates :number_stages, presence: true
  validates :start_time, presence: true
  validates :finish_time, presence: true
  validates :status, presence: true

  validates_with DateValidator

  enum status: { inactive: 0, active: 1, cancelled: 2, completed: 3 }

  scope :ordered_by_id, -> { order(id: :asc) }
  scope :implementer_or_author, lambda { |user|
                                  left_joins(:implementers)
                                    .where(contracts: { implementer_id: user.id })
                                    .or(left_joins(:implementers)
                                            .where(projects: { author_id: user.id }))
                                }
end
