# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, :confirmable,
         :trackable

  has_many :own_contracts, foreign_key: :user_id, class_name: 'Contract', dependent: :destroy,
                           inverse_of: :author
  has_many :implementer_contracts, foreign_key: :implementer_id, class_name: 'Contract', dependent: :destroy,
                                   inverse_of: :implementer
  has_many :own_projects, foreign_key: :author_id, class_name: 'Project', dependent: :destroy, inverse_of: :author
  has_many :implementer_projects, through: :implementer_contracts, source: :project
  has_many :achievement_users, dependent: :destroy
  has_many :achievements, through: :achievement_users
  has_many :tasks, dependent: :destroy
  has_many :reports, dependent: :destroy
  has_many :authorization_dates, dependent: :destroy
  has_many :integration_service_tokens, dependent: :destroy
  has_many :slack_tokens, dependent: :destroy
  has_many :comments, foreign_key: :author_id, dependent: :destroy, inverse_of: :author

  has_one :authorization_statistic, dependent: :destroy
  has_one_attached :avatar

  validates :first_name, presence: true
  validates :patronymic, presence: true
  validates :last_name, presence: true

  enum role: { developer: 0, manager: 1, admin: 2 }

  scope :ordered_by_last_name, -> { order(last_name: :asc) }
end
