# frozen_string_literal: true

class AuthorizationDate < ApplicationRecord
  belongs_to :user
end
