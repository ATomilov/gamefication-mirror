# frozen_string_literal: true

class Contract < ApplicationRecord
  belongs_to :project
  belongs_to :author, class_name: 'User', foreign_key: :user_id, inverse_of: :own_contracts
  belongs_to :implementer, class_name: 'User', foreign_key: :implementer_id, inverse_of: :implementer_contracts

  validates :implementer, uniqueness: { scope: :project }
  validates :start_time, presence: true
  validates :finish_time, presence: true

  delegate :title, to: :project, prefix: true, allow_nil: true

  scope :implementer_or_author, ->(user) { where(user_id: user).or(where(implementer_id: user)) }

  validates_with DateValidator
end
