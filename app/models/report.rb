# frozen_string_literal: true

class Report < ApplicationRecord
  belongs_to :project
  belongs_to :user
  has_many_attached :files

  validates :title, presence: true, length: { minimum: 5 }, uniqueness: { scope: :user_id }
  validates :body, presence: true
  validates :amount_time, presence: true

  delegate :title, :finish_time, :author, :slack_notification_settings, to: :project, prefix: true, allow_nil: true
  delegate :url_helpers, to: 'Rails.application.routes'

  validates_with AssociativeProjectDateValidator

  def structure_for_slack
    { user: user.decorate.name_with_initial,
      title: title,
      body: body,
      amount_time: amount_time,
      project: "<#{url_helpers.project_url(project)}|#{project_title}>",
      project_deadline_date: ConvertTimeService.time_to_str_us(project_finish_time) }
  end
end
