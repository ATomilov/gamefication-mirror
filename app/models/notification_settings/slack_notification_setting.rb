# frozen_string_literal: true

class SlackNotificationSetting < NotificationSetting
  store_accessor :data, %i[channels]
end
