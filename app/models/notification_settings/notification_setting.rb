# frozen_string_literal: true

class NotificationSetting < ApplicationRecord
  include Stiable

  serialize :data, HashSerializer

  belongs_to :project
  belongs_to :token, class_name: 'IntegrationServiceToken', foreign_key: :integration_service_token_id,
                     inverse_of: :notification_setting
  belongs_to :author, class_name: 'User'

  validates :token, uniqueness: { scope: :project }
  validates :type, inclusion: NotificationSetting.types

  delegate :title, to: :project, prefix: true, allow_nil: true
  delegate :properties, to: :token, prefix: true, allow_nil: true
end
