# frozen_string_literal: true

class AuthorizationAchievement < Achievement
  store_accessor :success_condition, %i[need_weeks]
end
