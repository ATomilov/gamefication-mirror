# frozen_string_literal: true

class Achievement < ApplicationRecord
  include Stiable

  before_create :default_icon, unless: :icon_attached?

  serialize :success_condition, HashSerializer

  has_many :achievement_users, dependent: :destroy
  has_many :users, through: :achievement_users

  has_one_attached :icon

  validates :title, presence: true, length: { minimum: 5 }, uniqueness: true
  validates :description, presence: true
  validates :type, inclusion: Achievement.types

  enum achievement_group: { everyone: 0, developer: 1, manager: 2 }, _prefix: :for

  private

  def default_icon
    icon.attach(io: File.open(path_to_icon), filename: 'default-image.png', content_type: 'image/png')
  end

  def icon_attached?
    icon.attached?
  end

  def path_to_icon
    Rails.root.join('app', 'assets', 'images', '100x100.png')
  end
end
