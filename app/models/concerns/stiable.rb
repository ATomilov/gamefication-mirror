# frozen_string_literal: true

module Stiable
  extend ActiveSupport::Concern

  class_methods do
    def types
      @types ||= distinct.pluck(:type)
    end
  end
end
