# frozen_string_literal: true

module Commentable
  extend ActiveSupport::Concern

  included do
    has_many :comments, as: :commentable, dependent: :destroy
  end

  def ordered_comments(order = :ASC)
    comments.order(created_at: order)
  end
end
