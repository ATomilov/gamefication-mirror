# frozen_string_literal: true

class DateValidator < ActiveModel::Validator
  def validate(record)
    return unless record.start_time.present? && record.finish_time.present?

    if record.finish_time < record.start_time
      record.errors.add(:finish_time, 'must be after the start time')
    elsif record.finish_time < Date.current
      record.errors.add(:finish_time, "can't be in the past")
    end
  end
end
