# frozen_string_literal: true

class AssociativeProjectDateValidator < ActiveModel::Validator
  def validate(record)
    return if record.project_finish_time.blank?

    record.errors.add(:project_finish_time, "can't be in the past") if record.project_finish_time < Date.current
  end
end
