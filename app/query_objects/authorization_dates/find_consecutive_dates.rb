# frozen_string_literal: true

module AuthorizationDates
  class FindConsecutiveDates < BaseQueryObject
    CONSECUTIVE_DATES = <<~SQL
        WITH
        dates(date) AS (
          SELECT DISTINCT CAST(date AS DATE)
          FROM authorization_dates
          WHERE authorization_dates.user_id = :user_id
          AND date_part('dow', date) not in (0,6)
      	  AND date > (current_timestamp - interval :days day)
        ),
        groups AS (
          SELECT
            ROW_NUMBER() OVER (ORDER BY date) AS rn,
            date - (ROW_NUMBER() OVER (ORDER BY date) * interval '1' day) AS grp,
            date
          FROM dates
        )
      SELECT
        COUNT(*) AS consecutive_days,
        MIN(date) AS min_date,
        date_part('dow', MIN(date)) AS min_date_dow,
        MAX(date) AS max_date,
        date_part('dow', MAX(date)) AS max_date_dow
      FROM groups
      GROUP BY grp
      HAVING date_part('dow', MAX(date)) = 5 OR date_part('dow', MIN(date)) = 1
      ORDER BY min_date desc
    SQL

    param :user

    def call
      dates_group
    end

    private

    def dates_group
      @dates_group ||= AuthorizationDate.find_by_sql([CONSECUTIVE_DATES, user_id: user.id, days: '30'])
    end
  end
end
