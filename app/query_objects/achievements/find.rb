# frozen_string_literal: true

module Achievements
  class Find < BaseQueryObject
    param :user
    param :affiliation

    def call
      select_by_params(affiliation)
    end

    private

    def select_by_params(params)
      if params == 'Mine'
        user.achievements
      elsif params == 'All'
        Achievement.all
      else
        user.achievements
      end
    end
  end
end
