# frozen_string_literal: true

class ContractsController < ApplicationController
  before_action :set_contract, only: %i[show edit update destroy]
  before_action :authorize_contract, only: %i[index new create]

  def index
    @contracts = policy_scope(Contract).paginate(page: params[:page], per_page: 5)
  end

  def show; end

  def new
    @contract = Contract.new
  end

  def edit; end

  def create
    @contract = Contract.new(contract_params.merge(author: current_user))

    if @contract.save
      redirect_to contract_url(@contract)
    else
      render 'new'
    end
  end

  def update
    if @contract.update(contract_params)
      redirect_to contract_url(@contract)
    else
      render 'edit'
    end
  end

  def destroy
    @contract.destroy

    redirect_to contracts_path
  end

  private

  def set_contract
    @contract = authorize policy_scope(Contract).find(params[:id])
  end

  def contract_params
    params.require(:contract).permit(:user_id, :project_id, :start_time, :finish_time, :implementer_id)
  end

  def authorize_contract
    authorize Contract
  end
end
