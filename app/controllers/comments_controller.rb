# frozen_string_literal: true

class CommentsController < ApplicationController
  before_action :find_commentable
  before_action :set_comment, only: %i[edit update destroy]
  before_action :authorize_comment, only: %i[new create]

  def new
    @comment = Comment.new
  end

  def edit
    respond_to do |format|
      format.html {}
      format.js
    end
  end

  def create
    @comment = @commentable.comments.new(comment_params.merge(author: current_user))

    respond_to do |format|
      if @comment.save
        render_success_respond(format)
      else
        render_fail_respond(format)
      end
    end
  end

  def update
    respond_to do |format|
      if @comment.update(comment_params)
        redirect_back_html(format, request.referer, notice: t('comments.edit'))
      else
        redirect_back_html(format, request.referer, error: t('comments.not_edit'))
      end
      format.js
    end
  end

  def destroy
    @comment.destroy
    respond_to do |format|
      redirect_back_html(format, request.referer, notice: t('comments.delete'))
      format.js
    end
  end

  private

  def set_comment
    @comment = authorize Comment.find(params[:id])
  end

  def comment_params
    params.require(:comment).permit(:content, files: [])
  end

  def find_commentable
    @commentable = Comment.find_by(id: params[:comment_id]) if params[:comment_id]
    @commentable = Task.find_by(id: params[:task_id]) if params[:task_id]
  end

  def authorize_comment
    authorize Comment
  end

  def render_success_respond(format)
    redirect_back_html(format, request.referer, notice: t('comments.create'))
    format.js { render 'reply.js.erb' if @commentable.is_a? Comment }
  end

  def render_fail_respond(format)
    redirect_back_html(format, request.referer, error: t('comments.not_create'))
    format.js
  end

  def redirect_back_html(format, link, message = {})
    format.html { redirect_back fallback_location: link, key: message[:key] }
  end
end
