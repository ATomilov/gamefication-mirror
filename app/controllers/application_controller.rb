# frozen_string_literal: true

class ApplicationController < ActionController::Base
  respond_to :html, :json, :js
  protect_from_forgery
  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :add_authorization_date
  before_action :set_locale

  include Pundit

  rescue_from Pundit::NotAuthorizedError, with: :redirect_not_authorized_user

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: %i[first_name patronymic last_name])
    devise_parameter_sanitizer.permit(:account_update, keys: %i[avatar first_name patronymic last_name])
  end

  def redirect_not_authorized_user
    flash[:error] = t('common.pundit.error')
    redirect_to root_path
  end

  def add_authorization_date
    Users::AddAuthorizationDate.call(current_user) unless current_user.nil?
  end

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end
end
