# frozen_string_literal: true

class TasksController < ApplicationController
  before_action :set_task, only: %i[show edit update destroy]
  before_action :authorize_task, only: %i[index new create]

  def index
    @tasks = policy_scope(Task).paginate(page: params[:page], per_page: 5)
  end

  def show; end

  def new
    @task = Task.new
  end

  def edit; end

  def create
    @task = Task.new(task_params.merge(user: current_user))

    if @task.save
      redirect_to task_url(@task)
    else
      render 'new'
    end
  end

  def update
    if @task.update(task_params)
      redirect_to task_url(@task)
    else
      render 'edit'
    end
  end

  def destroy
    @task.destroy

    redirect_to tasks_path
  end

  private

  def set_task
    @task = authorize policy_scope(Task).find(params[:id])
  end

  def task_params
    params.require(:task).permit(:title, :body, :start_time, :finish_time, :status, :project_id, :user_id,
                                 :implementer_id, files: [])
  end

  def authorize_task
    authorize Task
  end
end
