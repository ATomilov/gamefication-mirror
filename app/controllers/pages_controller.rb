# frozen_string_literal: true

class PagesController < ApplicationController
  layout 'application'

  def slack_token; end
end
