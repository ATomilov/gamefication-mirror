# frozen_string_literal: true

class SlackController < ApplicationController
  before_action :ensure_no_error
  before_action -> { ensure_ok(response_data) }

  def callback
    if update_user_slack_token(response_data)
      redirect_to root_path
    else
      # TODO: add translate for error update user slack token
      render text: "Oops! There was a problem."
    end
  end

  private

  def update_user_slack_token(data)
    slack_token = SlackToken.new(user_id: current_user.id)
    slack_token.token_body = EncryptionService.encrypt(data['access_token'])
    slack_token.properties = slack_token_data(data)
    slack_token.save!
  end

  def slack_token_data(data)
    { team_id: data['team_id'],
      team_name: data['team_name'],
      webhook_url: EncryptionService.encrypt(data['incoming_webhook']['url']),
      channel: data['incoming_webhook']['channel'] }
  end

  def redirect_url
    "http://localhost:3000/auth/callback"
  end

  def response_data
    client = Slack::Web::Client.new
    @response ||= client.oauth_access(
      client_id: Rails.application.credentials.slack_webhook[:client_id],
      client_secret: Rails.application.credentials.slack_webhook[:client_secret],
      code: params[:code],
      redirect_uri: redirect_url
    )
  end

  def ensure_no_error
    return unless params[:error]

    message = if params[:error] == 'access_denied'
                'You canceled get Slack token'
              else
                'An error occurred when getting Slack token'
              end
    redirect_to root_path, alert: message
  end

  def ensure_ok(data)
    return if data['ok']

    message = 'An error occurred when getting Slack token'
    redirect_to root_path, alert: message
  end
end
