# frozen_string_literal: true

class ReportsController < ApplicationController
  before_action :set_report, only: %i[show edit update destroy]
  before_action :authorize_report, only: %i[index new create]

  def index
    @reports = policy_scope(Report.paginate(page: params[:page], per_page: 5))
  end

  def show; end

  def new
    @report = Report.new
  end

  def edit; end

  def create
    @report = Report.new(report_params.merge(user: current_user))

    if @report.save
      redirect_to report_url(@report)
      send_notification_about_report(@report)
    else
      render 'new'
    end
  end

  def update
    if @report.update(report_params)
      redirect_to report_url(@report)
    else
      render 'edit'
    end
  end

  def destroy
    @report.destroy

    redirect_to reports_path
  end

  private

  def set_report
    @report = authorize policy_scope(Report).find(params[:id])
  end

  def report_params
    params.require(:report).permit(:title, :body, :amount_time, :attachment,
                                   :project_id, :user_id, files: [])
  end

  def authorize_report
    authorize Report
  end

  def report_files(report)
    return unless report.files.attached?

    hash_to_merge = Hash.new([])
    report.files.each_with_index do |file, index|
      hash_to_merge["Attachment #{index + 1}"] = "<#{polymorphic_url(file)}|#{file.filename}>"
    end
    hash_to_merge
  end

  def report_data(report)
    report.structure_for_slack.merge(report_files(report).to_h)
  end

  def send_notification_about_report(report)
    report.project_slack_notification_settings.each do |slack_notification|
      webhook_url = EncryptionService.decrypt(slack_notification.token_properties[:webhook_url])
      SendSlackNotificationJob.perform_later(webhook_url, report_data(report))
    end
  end
end
