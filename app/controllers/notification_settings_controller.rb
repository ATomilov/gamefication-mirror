# frozen_string_literal: true

class NotificationSettingsController < ApplicationController
  before_action :set_notification_setting, only: %i[show edit update destroy]
  before_action :authorize_notification_setting, only: %i[index new create]

  def index
    @notification_settings = policy_scope(NotificationSetting).paginate(page: params[:page], per_page: 5)
  end

  def show; end

  def new
    @notification_setting = NotificationSetting.new
  end

  def edit; end

  def create
    SetSafeStiType.call(notification_setting_params[:type], params[:notification_setting], 'NotificationSetting')

    @notification_setting = NotificationSetting.new(notification_setting_params.merge(author: current_user))

    if @notification_setting.save
      redirect_to notification_setting_url(@notification_setting.becomes(NotificationSetting))
    else
      render 'new'
    end
  end

  def update
    if @notification_setting.update(notification_setting_params)
      redirect_to notification_setting_url(@notification_setting.becomes(NotificationSetting))
    else
      render 'edit'
    end
  end

  def destroy
    @notification_setting.destroy

    redirect_to notification_settings_path
  end

  private

  def set_notification_setting
    @notification_setting = policy_scope(NotificationSetting).find(params[:id])
  end

  def notification_setting_params
    params.require(:notification_setting).permit(:project_id, :integration_service_token_id, :type)
  end

  def authorize_notification_setting
    authorize NotificationSetting
  end
end
