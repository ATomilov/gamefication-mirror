# frozen_string_literal: true

class ProjectsController < ApplicationController
  before_action :set_project, only: %i[show edit update destroy]
  before_action :authorize_project, only: %i[index new create]

  def index
    @projects = policy_scope(Project.ordered_by_id).paginate(page: params[:page], per_page: 5)
  end

  def show; end

  def new
    @project = Project.new
  end

  def edit; end

  def create
    @project = Project.new(project_params.merge(author: current_user))

    if @project.save
      redirect_to project_url(@project)
    else
      render 'new'
    end
  end

  def update
    if @project.update(project_params)
      redirect_to project_url(@project)
    else
      render 'edit'
    end
  end

  def destroy
    @project.destroy

    redirect_to projects_path
  end

  private

  def set_project
    @project = authorize policy_scope(Project).find(params[:id])
  end

  def project_params
    params.require(:project).permit(:title, :body, :attachment, :number_stages,
                                    :start_time, :finish_time, :status, files: [])
  end

  def authorize_project
    authorize Project
  end
end
