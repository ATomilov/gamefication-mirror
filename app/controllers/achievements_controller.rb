# frozen_string_literal: true

class AchievementsController < ApplicationController
  before_action :set_achievement, only: %i[show edit update destroy]
  before_action :authorize_achievement, only: %i[index new create]

  def index
    @achievements = Achievements::Find.call(current_user, params[:affiliation])
    @achievements = @achievements.paginate(page: params[:page], per_page: 5)
  end

  def show; end

  def new
    @achievement = Achievement.new
  end

  def edit; end

  def create
    SetSafeStiType.call(achievement_params[:type], params[:achievement], 'Achievement')

    @achievement = Achievement.new(achievement_params)

    if @achievement.save
      redirect_to achievement_url(@achievement.becomes(Achievement))
    else
      render 'new'
    end
  end

  def update
    if @achievement.update(achievement_params)
      redirect_to achievement_url(@achievement.becomes(Achievement))
    else
      render 'edit'
    end
  end

  def destroy
    @achievement.destroy

    redirect_to achievements_path
  end

  private

  def set_achievement
    @achievement = policy_scope(Achievement).find(params[:id])
  end

  def achievement_params
    params.require(:achievement).permit(:title, :description, :type, :icon)
  end

  def authorize_achievement
    authorize Achievement
  end
end
