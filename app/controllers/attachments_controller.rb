# frozen_string_literal: true

class AttachmentsController < ApplicationController
  def destroy
    @attachment = ActiveStorage::Attachment.find(params[:id])
    @attachment.purge

    respond_to do |format|
      format.html { redirect_back(fallback_location: request.referer) }
      format.js
    end
  end
end
