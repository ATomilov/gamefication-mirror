# frozen_string_literal: true

class AuthorizationDatesRepository < BaseRepository
  param :user

  def dates_last_days(days)
    user.authorization_dates.distinct.select("date::date")
        .where("date_part('dow', date) not in (0,6)")
        .where("date > (current_timestamp - interval '?' day)", days)
  end

  def consecutive_days
    number_consecutive_days.to_i
  end

  def full_weeks_last_n_weeks(number_week)
    full_time_weeks_last_n_week(number_week).to_i
  end

  private

  def auth_dates
    @auth_dates ||= AuthorizationDates::FindConsecutiveDates.call(user)
  end

  def number_consecutive_days
    auth_dates.first.consecutive_days unless auth_dates.empty?
  end

  def full_time_weeks_last_n_week(number_week)
    full_time_weeks = []
    auth_dates.each do |date_range|
      full_time_weeks << date_range if right_monday_and_friday?(date_range.min_date, date_range.max_date, number_week)
    end
    full_time_weeks.size
  end

  def right_monday?(date, number_week)
    date.monday? && (date >= Date.current.beginning_of_week - number_week.week)
  end

  def right_friday?(date, number_week)
    date.friday? && (date >= Date.current.end_of_week(:saturday) - number_week.week)
  end

  def right_monday_and_friday?(monday, friday, number_week)
    right_monday?(monday, number_week) && right_friday?(friday, number_week)
  end
end
