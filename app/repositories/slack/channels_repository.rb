# frozen_string_literal: true

module Slack
  class ChannelsRepository < BaseRepository
    param :user
    def conversations
      conversations_list_slice
    end

    private

    def slack_client
      # TODO: add decryption users' tokens when get it from db
      @slack_client ||= Slack::Web::Client.new(token: user_slack_token(user))
    end

    def conversations_list
      slack_client.users_conversations[:channels]
    end

    def conversations_list_slice
      conversations_names_list = []
      conversations_list.each do |conversation_list_elem|
        conversations_names_list << conversation_list_elem.slice('name')['name']
      end
      conversations_names_list.sort
    end

    def user_slack_token(user)
      EncryptionService.decrypt(user.slack_tokens.last.token_body)
    end
  end
end
