$(document).ajaxSend(function(e, xhr, options) {
 var token = $("meta[name='csrf-token']").attr("content");
  xhr.setRequestHeader("X-CSRF-Token", token);
});

/**
 * Show reply form by click
 */

$(document).on('click', '#reply-comment', function (e) {
  var ReplyCommentForm = $(this).parent().parent().parent().find('#reply-comment-form');

  // var method = 'get'
  // var CommentUrl = $(this).attr('href');

  // $.ajax({
  //   method: method,
  //   url: CommentUrl,
  //   dataType: 'script',
  // });

  // EditCommentFormBlock.toggle();
  
  e.preventDefault();


  if (ReplyCommentForm.hasClass('active')) {
    ReplyCommentForm.hide();
    ReplyCommentForm.removeClass('active')
  } else {
    ReplyCommentForm.addClass('active');
    ReplyCommentForm.show();
  }
});

/**
 * Load comment's edit form on ajax
 */

$(document).on('click', '#edit-comment', function (e) {
  var EditCommentFormBlock = $(this).parent().parent().parent().find('#edit-comment-form');
  var method = 'get'
  var CommentUrl = $(this).attr('href');

  $.ajax({
    method: method,
    url: CommentUrl,
    dataType: 'script',
  });

  EditCommentFormBlock.toggle();
  
  e.preventDefault();
});


/**
 * Add new comment ajax
 */

$("form#add-new-comment").submit(function (event) {
  event.preventDefault();

  var action = $(this).attr('action');
  var method = $(this).attr('method');
  var data = $(this).serializeArray();

  $.ajax({
    method: method,
    url: action,
    data: data,
    dataType: 'script',
  });
});

/**
 * Delete comment ajax
 */

$(document).on('click', 'a#delete-comment', function (e) {
  var CommentUrl = $(this).attr('href');

  var method = 'delete'

  $.ajax({
    method: method,
    url: CommentUrl,
    dataType: 'script',
  });

  e.preventDefault();
});

/**
 * Add reply 
 */

// $("form#add-reply").submit(function(event){
//   event.preventDefault();

//   var action = $(this).attr('action');
//   var method = $(this).attr('method');
//   var data = $(this).serializeArray();

//   alert(action + " " + method);

//   event.preventDefault();

//   // $.ajax({
//     //   method: method,
//     //   url: action,
//     //   data: data,
//     //   dataType: 'script'
//     // });
// });

/**
 * Delete attachment ajax
 */

$(document).on('click', 'a.delete', function (e) {
  var AttachmentUrl = $(this).attr('href');

  var method = 'delete'

  $.ajax({
    method: method,
    url: AttachmentUrl,
    dataType: 'script',
  });
  e.preventDefault();
});