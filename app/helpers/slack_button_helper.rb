# frozen_string_literal: true

module SlackButtonHelper
  def connect_to_slack_button
    content_tag 'a', href: "#{slack_oauth_link}?#{slack_params}" do
      concat(image_tag('https://platform.slack-edge.com/img/add_to_slack.png', alt: 'Add to Slack'))
    end
  end

  private

  def slack_params
    "client_id=#{client_id}&scope=#{slack_scope}"
  end

  def slack_oauth_link
    'https://slack.com/oauth/authorize'
  end

  def client_id
    Rails.application.credentials.slack_webhook[:client_id]
  end

  def slack_scope
    'incoming-webhook,channels:read,team:read'
  end
end
