# frozen_string_literal: true

module TableRowColorHelper
  def user_color_row(user)
    if current_user == user
      "has-background-grey-lighter"
    else
      "has-background-white"
    end
  end
end
