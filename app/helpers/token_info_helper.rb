# frozen_string_literal: true

module TokenInfoHelper
  def channel_and_workspace_from_parent_instance(obj)
    obj.token.decorate.channel_with_workspace
  end

  def token_type_from_parent_instance(obj)
    obj.token.type.underscore.humanize
  end
end
