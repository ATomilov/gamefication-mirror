# frozen_string_literal: true

module UserAvatarHelper
  def user_gravatar(user, data = {})
    gravatar_id = Digest::MD5.hexdigest(user.email.downcase)
    gravatar_url = "http://secure.gravatar.com/avatar/#{gravatar_id}?s=100"
    image_tag(gravatar_url, alt: user.decorate.name_with_initial, class: data[:class])
  end
end
