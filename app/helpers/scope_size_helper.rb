# frozen_string_literal: true

module ScopeSizeHelper
  def user_projects_size(obj)
    Project.implementer_or_author(obj).size unless obj.nil?
  end

  def user_contracts_size(obj)
    Contract.implementer_or_author(obj).size unless obj.nil?
  end
end
