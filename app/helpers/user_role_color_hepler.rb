# frozen_string_literal: true

module UserRoleColorHepler
  def user_role_css_class(user)
    if user.admin?
      'is-danger'
    elsif user.manager?
      'is-warning'
    else
      'is-success'
    end
  end
end
