# frozen_string_literal: true

module UserNameHelper
  def author_name_from_parent_instance(obj)
    obj.author.decorate.name_with_initial
  end

  def implementer_from_parent_instance(obj)
    obj.implementer.decorate.name_with_initial
  end

  def user_name_from_instance(obj)
    obj.decorate.name_with_initial
  end
end
