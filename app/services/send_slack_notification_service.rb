# frozen_string_literal: true

class SendSlackNotificationService < BaseServiceObject
  GOOD = 'good'
  WARNING = 'warning'
  DANGER = 'danger'
  WARNING_DAYS_INTERVAL = (4..15).freeze
  DANGER_DAYS_INTERVAL = (0..3).freeze

  param :url
  param :data, proc(&:to_h)

  # TODO: add translation for message's text
  def call
    @params = generate_payload(message_params(data))
    begin
      Net::HTTP.post_form(URI(url), @params)
    rescue StandardError => e
      Rails.logger.error("SlackBotService: Error when sending: #{e.message}")
    end
  end

  def deliver
    Net::HTTP.post_form(URI(url), @params)
  rescue StandardError => e
    Rails.logger.error("SlackBotService: Error when sending: #{e.message}")
  end

  private

  def generate_payload(params)
    { payload: params.to_json }
  end

  def message_params(data)
    params = message_structure(data)
    add_items_in_loop(data, params)
  end

  def add_items_in_loop(data, params)
    data.each do |key, value|
      params[:attachments].first[:fields] += [{ title: key.to_s.humanize, value: value, short: key != data.keys.first }]
    end
    params
  end

  def message_structure(data)
    { attachments: [{
      title: 'New report was created',
      fallback: 'New report was created',
      color: color_for_message(ConvertTimeService.str_to_time(data[:project_deadline_date])),
      fields: []
    }] }
  end

  def color_for_message(date)
    days_difference = GetDaysService.days_between_dates(date, Date.current)
    if days_difference.between?(WARNING_DAYS_INTERVAL.min, WARNING_DAYS_INTERVAL.max)
      WARNING
    elsif days_difference.between?(DANGER_DAYS_INTERVAL.min, DANGER_DAYS_INTERVAL.max)
      DANGER
    else
      GOOD
    end
  end
end
