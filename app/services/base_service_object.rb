# frozen_string_literal: true

class BaseServiceObject
  extend Dry::Initializer

  def self.call(*args, &block)
    new(*args, &block).call
  end

  def new(*args)
    args << args.pop.symbolize_keys if args.last.is_a?(Hash)
    super(*args)
  end
end
