# frozen_string_literal: true

module Statistics
  class Create < BaseServiceObject
    param :user
    param :type, proc(&:strip)
    param :data_keys_values, proc(&:to_h)

    def call
      return if class_name_from_type_nil?

      statistic = class_name_from_type.find_or_initialize_by(user_id)
      statistic.data = data_keys_values
      statistic.save!
    end

    private

    def user_id
      { user_id: user.id }
    end

    def class_name_from_type_nil?
      class_name_from_type.nil?
    end

    def class_name_from_type
      "#{type}_statistic".classify.safe_constantize
    end
  end
end
