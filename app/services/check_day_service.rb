# frozen_string_literal: true

class CheckDayService
  class << self
    def today_workday?
      (1..5).to_a.include?(GetDateService.today.wday)
    end
  end
end
