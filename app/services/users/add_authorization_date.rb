# frozen_string_literal: true

module Users
  class AddAuthorizationDate < BaseServiceObject
    WEEK_SIZE_DAYS = 7

    param :user

    def call
      return if user_was_active_today?

      authorization_date
      add_sign_in_date(authorization_date.date)
    end

    private

    def add_sign_in_date(date)
      Redis.current.hmset("user:#{user.id}",
                          Rails.configuration.application.dig(:users, :current_sign_in),
                          date.strftime(Rails.configuration.application.dig(:date, :format_us)).to_s)

      Statistics::Create.call(user, 'authorization', data)
    end

    def authorization_date
      @authorization_date ||= AuthorizationDate.create(user_id: user.id, date: Time.current.change(usec: 0))
    end

    def data
      { last_one_week_days: weeks_before(1),
        last_two_weeks_days: weeks_before(2),
        last_three_weeks_days: weeks_before(3),
        last_four_weeks_days: weeks_before(4),
        consecutive_days: consecutive_days,
        last_one_week_consecutive_weeks: consecutive_weeks_before_weeks(1),
        last_two_week_consecutive_weeks: consecutive_weeks_before_weeks(2),
        last_three_week_consecutive_weeks: consecutive_weeks_before_weeks(3),
        last_four_week_consecutive_weeks: consecutive_weeks_before_weeks(4) }
    end

    def user_current_sign_in_str
      Redis.current.hget("user:#{user.id}", Rails.configuration.application.dig('users', 'current_sign_in'))
    end

    def user_was_active_today?
      user_current_sign_in_str == GetConvertedDate.today
    end

    def number_authorizations
      @authorization_date_repository ||= AuthorizationDatesRepository.new(user)
    end

    def weeks_before(number)
      number_authorizations.dates_last_days(WEEK_SIZE_DAYS * number).size
    end

    def consecutive_days
      number_authorizations.consecutive_days
    end

    def consecutive_weeks_before_weeks(number)
      number_authorizations.full_weeks_last_n_weeks(number).to_i
    end
  end
end
