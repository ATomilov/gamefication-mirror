# frozen_string_literal: true

module Users
  class AddAchievement < BaseServiceObject
    param :user
    param :achievement

    def call
      user.achievements << achievement
    rescue ActiveRecord::RecordInvalid
      nil
    end
  end
end
