# frozen_string_literal: true

class GetDateService < BaseServiceObject
  param :user
  class << self
    def today
      Date.current
    end

    def yesterday
      Date.yesterday
    end

    def prior_weekday(date, weekday)
      weekday_index = Date::DAYNAMES.reverse.index(weekday)
      days_before = (date.wday + weekday_index) % 7 + 1
      date.to_date - days_before
    end
  end
end
