# frozen_string_literal: true

class ConvertTimeService
  class << self
    def time_to_str_us(date)
      date.strftime(Rails.configuration.application.dig(:date, :format_us)).to_s
    end

    def time_to_str_eu(date)
      date.strftime(Rails.configuration.application.dig(:date, :format_eu)).to_s
    end

    def str_to_time(str)
      Date.strptime(str, Rails.configuration.application.dig(:date, :format_us))
    end
  end
end
