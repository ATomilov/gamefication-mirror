# frozen_string_literal: true

class GetDaysService
  def self.days_between_dates(first_date, second_date)
    (first_date - second_date).to_i
  end
end
