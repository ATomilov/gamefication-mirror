# frozen_string_literal: true

class SetSafeStiType < BaseServiceObject
  param :selected_type
  param :hash_to_merge
  param :needed_type, proc(&:to_s)

  def call
    if selected_type.classify.safe_constantize.nil?
      hash_to_merge.merge!(type: needed_type)
    else
      hash_to_merge.merge!(type: selected_type)
    end
  end
end
