# frozen_string_literal: true

class GetConvertedDate
  class << self
    def yesterday
      ConvertTimeService.time_to_str_us(GetDateService.yesterday)
    end

    def today
      ConvertTimeService.time_to_str_us(GetDateService.today)
    end
  end
end
