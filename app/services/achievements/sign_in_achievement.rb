# frozen_string_literal: true

module Achievements
  class SignInAchievement < BaseServiceObject
    LEVELS = (1..4).to_a.freeze

    param :user_id

    def call
      user = User.find(user_id)
      check_condition = Achievements::CheckSignInAchievement.new(user)
      LEVELS.each do |level|
        AuthorizationAchievement.for_everyone.find_each do |achievement|
          next unless check_condition.unlocked_on_level?(level) && achievement.need_weeks == level.to_s
          next if Users::AddAchievement.call(user, achievement).nil?
        end
      end
    end
  end
end
