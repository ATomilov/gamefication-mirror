# frozen_string_literal: true

module Achievements
  class CheckSignInAchievement < BaseServiceObject
    param :user

    def unlocked_on_level?(level)
      user_consecutive_weeks(level) == level.to_s
    end

    private

    def user_auth_statistic
      @user_auth_statistic ||= user.authorization_statistic.data
    end

    def user_consecutive_weeks(number_weeks)
      user_auth_statistic["last_#{number_weeks.to_words}_week_consecutive_weeks"]
    end
  end
end
