# frozen_string_literal: true

module Projects
  class IndexValueObject < BaseValueObject
    param :user

    def ordered_projects
      @ordered_projects ||= projects_scope
    end

    private

    def projects_scope
      ProjectPolicy::Scope.new(user, Project.ordered_by_id).resolve
    end
  end
end
