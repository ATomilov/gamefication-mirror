# frozen_string_literal: true

module IntegrationServiceTokens
  class IndexValueObject < BaseValueObject
    param :user

    def non_decorated_tokens
      @non_decorated_tokens ||= tokens_scope
    end

    def decorated_tokens
      @decorated_tokens ||= tokens_scope.decorate
    end

    private

    def tokens_scope
      IntegrationServiceTokenPolicy::Scope.new(user, IntegrationServiceToken.all).resolve
    end
  end
end
