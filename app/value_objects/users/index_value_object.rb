# frozen_string_literal: true

module Users
  class IndexValueObject < BaseValueObject
    param :user

    def non_decorated_users
      @non_decorated_users ||= users_scope
    end

    def decorated_users
      @decorated_users ||= users_scope.decorate
    end

    private

    def users_scope
      UserPolicy::Scope.new(user, User.ordered_by_last_name).resolve
    end
  end
end
